/** @type {import('next').NextConfig} */
const path = require("path");

const nextConfig = {
  reactStrictMode: false,
  swcMinify: true,
  sassOptions: {
    includePaths: [path.join(__dirname, "styles")],
  },
  distDir: "_next",
  env: {
    HostName: "locallhost:2020",
    RegisterUser: "/api/account/registerUser",
    LoginUser: "/api/account/loginUser",
  },
};

module.exports = nextConfig;

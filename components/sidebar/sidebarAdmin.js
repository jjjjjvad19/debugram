import IconBadge from "../../tools/elemnts/icons/iconBadge";

const SidebarAdmin = ({ toggle }) => {
  return (
    <>
      <div className="fixed w-inherit h-100% bg-[#eee] select-none">
        <ul className="p-0 w-100% flex justify-center items-center flex-col py-2 gap-y-4">
          <li
            className={`p-2 cursor-pointer ${
              toggle ? "w-100% pr-5 flex justify-center items-center" : ""
            }`}
          >
            <div className="w-auto">
              <IconBadge title={"مدیریت مقاله"}>
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  fill="none"
                  viewBox="0 0 24 24"
                  strokeWidth={1.5}
                  stroke="currentColor"
                  className="w-6 h-6 text-[#F14A16]"
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    d="M15.75 17.25v3.375c0 .621-.504 1.125-1.125 1.125h-9.75a1.125 1.125 0 01-1.125-1.125V7.875c0-.621.504-1.125 1.125-1.125H6.75a9.06 9.06 0 011.5.124m7.5 10.376h3.375c.621 0 1.125-.504 1.125-1.125V11.25c0-4.46-3.243-8.161-7.5-8.876a9.06 9.06 0 00-1.5-.124H9.375c-.621 0-1.125.504-1.125 1.125v3.5m7.5 10.375H9.375a1.125 1.125 0 01-1.125-1.125v-9.25m12 6.625v-1.875a3.375 3.375 0 00-3.375-3.375h-1.5a1.125 1.125 0 01-1.125-1.125v-1.5a3.375 3.375 0 00-3.375-3.375H9.75"
                  />
                </svg>
              </IconBadge>
            </div>
            <h1
              className={`w-90% overflow-hidden text-ellipsis whitespace-nowrap ${
                toggle
                  ? "visible pl-5 pr-4 transition-all delay-100"
                  : "invisible w-0 h-0"
              }`}
            >
              مدیریت مقاله ها
            </h1>
          </li>
          <li
            className={`p-2  cursor-pointer ${
              toggle ? "w-100% pr-5 flex justify-center items-center" : ""
            }`}
          >
            <div className="w-auto">
              <IconBadge title={"مدیریت پیام ها"}>
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  fill="none"
                  viewBox="0 0 24 24"
                  strokeWidth={1.5}
                  stroke="currentColor"
                  className="w-6 h-6 text-[#4C3F91]"
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    d="M21.75 6.75v10.5a2.25 2.25 0 01-2.25 2.25h-15a2.25 2.25 0 01-2.25-2.25V6.75m19.5 0A2.25 2.25 0 0019.5 4.5h-15a2.25 2.25 0 00-2.25 2.25m19.5 0v.243a2.25 2.25 0 01-1.07 1.916l-7.5 4.615a2.25 2.25 0 01-2.36 0L3.32 8.91a2.25 2.25 0 01-1.07-1.916V6.75"
                  />
                </svg>
              </IconBadge>
            </div>
            <h1
              className={`w-90% overflow-hidden text-ellipsis whitespace-nowrap ${
                toggle
                  ? "visible pl-5 pr-4 transition-all delay-100"
                  : "invisible w-0 h-0"
              }`}
            >
              مدیریت پیام ها
            </h1>
          </li>
          <li
            className={`p-2  cursor-pointer ${
              toggle ? "w-100% pr-5 flex justify-center items-center" : ""
            }`}
          >
            <div className="w-auto">
              <IconBadge title={"مدیریت تم ها"}>
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  fill="none"
                  viewBox="0 0 24 24"
                  strokeWidth={1.5}
                  stroke="currentColor"
                  className="w-6 h-6 text-[#F0A500]"
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    d="M12 3v2.25m6.364.386l-1.591 1.591M21 12h-2.25m-.386 6.364l-1.591-1.591M12 18.75V21m-4.773-4.227l-1.591 1.591M5.25 12H3m4.227-4.773L5.636 5.636M15.75 12a3.75 3.75 0 11-7.5 0 3.75 3.75 0 017.5 0z"
                  />
                </svg>
              </IconBadge>
            </div>
            <h1
              className={`w-90% overflow-hidden text-ellipsis whitespace-nowrap ${
                toggle
                  ? "visible pl-5 pr-4 transition-all delay-100"
                  : "invisible w-0 h-0"
              }`}
            >
              مدیریت تم ها
            </h1>
          </li>
        </ul>
      </div>
    </>
  );
};

export default SidebarAdmin;

import Link from "next/link";

const WayURL = ({ data }) => {
  return (
    <>
      <ul className="flex justify-center items-center gap-x-1 mr-2">
        {data.map((val, index, arr) => (
          <>
            <li className="flex justify-center  items-center" key={index}>
              <Link href={"/"}>
                <a className="text-[#DC3535] hover:text-[#B01E68] transition-colors duration-200 ">
                  <p className="text-sm">{val.text}</p>
                </a>
              </Link>
            </li>
            <li>
              {index !== arr.length - 1 ? (
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  fill="none"
                  viewBox="0 0 24 24"
                  strokeWidth={1.5}
                  stroke="currentColor"
                  className="w-4 h-4 text-[#432C7A]"
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    d="M15.75 19.5L8.25 12l7.5-7.5"
                  />
                </svg>
              ) : (
                ""
              )}
            </li>
          </>
        ))}
      </ul>
    </>
  );
};

export default WayURL;

import { useEffect, useRef, useState } from "react";
import { useDispatch } from "react-redux";
import styles from "../../styles/components/device/deviceMode.module.scss";
import { $watched } from "../../StoreManager/Reducers/deviceSlice";

const DeviceMode = () => {
  const [refElement, setRefElement] = useState(undefined);
  const dispatch = useDispatch();
  const arrDevice = [
    { type: "Desktop", id: 1 },
    { type: "Tablet", id: 2 },
    { type: "Mobile", id: 3 },
  ];
  useEffect(() => {
    if (refElement) {
      setModeDevice();
      window.addEventListener("resize", () => {
        setModeDevice();
      });
    }
  });
  const setModeDevice = () => {
    var currentDevice = {};
    refElement.childNodes.forEach((item) => {
      var active = getComputedStyle(item).opacity;
      if (active == "1") {
        arrDevice.map((n) => {
          if (item.className.includes(n.type)) {
            currentDevice = n;
          }
        });
      }
    });
    dispatch($watched(currentDevice));
  };
  return (
    <>
      <div ref={setRefElement}>
        <div className={styles.Device__Desktop}></div>
        <div className={styles.Device__Mobile}></div>
        <div className={styles.Device__Tablet}></div>
      </div>
    </>
  );
};

export default DeviceMode;

import React, { useEffect } from "react";
import Image from "next/image";

const FooterPage = () => {
  useEffect(() => {}, []);
  return (
    <>
      <footer className="bottom-0  w-[100%] bg-back-3">
        <div className="md:container md:mx-auto" dir="rtl">
          <div className="pt-5">
            <h3 className="text-xl font-bold px-8 md:px-0">درباره سایت ما</h3>
            <p className="text-justify px-8   pt-3 md:px-0 tracking-tight">
              وقتی تازه شروع به یادگیری برنامه نویسی کردم. یکی از مشکلاتی که در
              فرآیند یادگیری داشتم، کمبود آموزش های خوب با پشتیبانی قابل قبول
              بود که باعث شد اون موقع تصمیم بگیرم اگر روزی توانایی مالی و فنی
              قابل قبولی داشتم یک وب سایت برای حل این مشکل راه اندازی کنم! و خب
              امروز آکادمی آموزش برنامه نویسی سبزلرن به عنوان یک آکادمی خصوصی
              فعالیت میکنه و این به این معنی هست که هر مدرسی اجازه تدریس در اون
              رو نداره و باید از فیلترینگ های خاص آکادمی سبزلرن رد شه! این به
              این معنی هست که ما برامون فن بیان و نحوه تعامل مدرس با دانشجو
              بسیار مهمه! ما در آکادمی سبزلرن تضمین پشتیبانی خوب و با کیفیت رو
              به شما میدیم . چرا که مدرسین وب سایت سبزلرن حتی برای پشتیبانی دوره
              های رایگان شون هم هزینه دریافت میکنند و متعهد هستند که هوای کاربر
              های عزیز رو داشته باشند
            </p>
          </div>

          <div className="grid grid-cols-2  gap-0  py-4 md:grid-cols-5 mt-10">
            {/* لینک های پر کاربرد */}
            <div className="text-center md:text-start">
              <h2 className="mb-3 text-xl  md:text-sm  font-bold text-gray-500 cursor-pointer hover:text-color-3 ">
                لینکهای پرکاربرد
              </h2>
              <ul className="text-gray-500  ">
                <li className="mb-4">
                  <a href="#" className=" hover:no-underline">
                    دوره های طلایی
                  </a>
                </li>
                <li className="mb-4">
                  <a href="#" className="hover:no-underline">
                    دوره‌های رایگان
                  </a>
                </li>
              </ul>
            </div>

            {/* تماس با ما */}
            <div className="text-center md:text-start">
              <h2 className="mb-3 text-xl  md:text-sm font-bold text-gray-500 cursor-pointer hover:text-color-3 ">
                تماس با ما
              </h2>
              <ul className="text-gray-500   ">
                <li className="mb-4">
                  <a href="#" className="hover:no-underline">
                    ارتباط با ما
                  </a>
                </li>
                <li className="mb-4">
                  <a href="#" className="hover:no-underline">
                    همکاری با ما
                  </a>
                </li>
              </ul>
            </div>

            {/* مقالات */}
            <div className="text-center md:text-start">
              <h2 className="mb-3 text-xl  md:text-sm  font-bold text-gray-500 cursor-pointer hover:text-color-3 ">
                مقالات
              </h2>
              <ul className="text-gray-500 ">
                <li className="mb-4">
                  <a href="#" className="hover:no-underline">
                    قوانین و مقررات
                  </a>
                </li>
                <li className="mb-4">
                  <a href="#" className="hover:no-underline">
                    آموزش پایتون
                  </a>
                </li>
              </ul>
            </div>

            {/* ارتلاط با ما */}
            <div className="text-center md:text-start">
              <h2 className="mb-3 text-xl  md:text-sm  font-bold text-gray-500 cursor-pointer hover:text-color-3 ">
                ارتباط ما:
              </h2>
              <ul className="text-gray-500 ">
                <li className="mb-4">
                  <a
                    href="#"
                    className="hover:no-underline text-[8px] xs:text-sm"
                  >
                    ایمیل : sabzlearn@gmail.com
                  </a>
                </li>
                <li className="mb-4">
                  <a
                    href="#"
                    className="hover:no-underline text-[8px] xs:text-sm"
                  >
                    شماره تماس : <span> 09334008385</span>
                  </a>
                </li>
              </ul>
            </div>

            {/* /image */}
            <div className="mx-auto col-span-2 md:col-auto ">
              <Image
                src="/star1.png"
                layout="fixed"
                alt="Picture of the author"
                width={150}
                height={150}
              />
            </div>
          </div>

          <hr className="bg-back-1 text-[#fff]  " />

          <div className="py-6 px-4 bg-gray-100 text-center text-[10px]  sm:text-sm  hover:font-bold   ">
            <p className="text-gray-500">
              کلیه حقوق مادی و معنوی برای وب سایت دیباگرم محفوظ است. 2020 -
              Copyright
            </p>
          </div>
        </div>
        <div className="flex items-end justify-end fixed bottom-0 right-0 mb-4 mr-4 z-10">
          <div>
            <a
              title="Buy me a beer"
              href="https://www.buymeacoffee.com/scottwindon"
              target="_blank"
              rel="noreferrer"
              className="block w-16 h-16 rounded-full transition-all shadow hover:shadow-lg transform hover:scale-110 hover:rotate-12"
            >
              <Image
                className="object-cover object-center w-full h-full rounded-full"
                src="/Images/images.png"
                alt="Picture of the author"
                objectFit="contain"
                layout="fill"
                width={50}
                height={50}
              />
            </a>
          </div>
        </div>
      </footer>
    </>
  );
};

export default FooterPage;

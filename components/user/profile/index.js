import Image from "next/image";
const Profile = () => {
  return (
    <>
      <div>
        <div className="flex flex-row mt-1">
          <div className="basis-3/4 mx-2 mt-2  ">
            <div className="text-end ">
              <p className=" font-bold text-2xl pl-2  font-sans">
                javad<spna className="px-2">Sargazi</spna>
              </p>
              <div className="flex justify-end mt-2 ">
                <spna className="pt-1 px-1">
                  آخرین بار بیش از1 سال پیش دیده شده است
                </spna>

                {/* icon clock */}
                <span className="pt-1 px-1">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 24 24"
                    stroke-width="1.5"
                    stroke="currentColor"
                    class="w-6 h-6"
                  >
                    <path
                      stroke-linecap="round"
                      stroke-linejoin="round"
                      d="M12 6v6h4.5m4.5 0a9 9 0 11-18 0 9 9 0 0118 0z"
                    />
                  </svg>
                </span>

                <span className="pt-1 px-1">عضو به مدت 13 سال و 6 ماه</span>
                {/* icon birthay */}
                <span>
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 24 24"
                    stroke-width="1.5"
                    stroke="currentColor"
                    class="w-6 h-6"
                  >
                    <path
                      stroke-linecap="round"
                      stroke-linejoin="round"
                      d="M12 8.25v-1.5m0 1.5c-1.355 0-2.697.056-4.024.166C6.845 8.51 6 9.473 6 10.608v2.513m6-4.87c1.355 0 2.697.055 4.024.165C17.155 8.51 18 9.473 18 10.608v2.513m-3-4.87v-1.5m-6 1.5v-1.5m12 9.75l-1.5.75a3.354 3.354 0 01-3 0 3.354 3.354 0 00-3 0 3.354 3.354 0 01-3 0 3.354 3.354 0 00-3 0 3.354 3.354 0 01-3 0L3 16.5m15-3.38a48.474 48.474 0 00-6-.37c-2.032 0-4.034.125-6 .37m12 0c.39.049.777.102 1.163.16 1.07.16 1.837 1.094 1.837 2.175v5.17c0 .62-.504 1.124-1.125 1.124H4.125A1.125 1.125 0 013 20.625v-5.17c0-1.08.768-2.014 1.837-2.174A47.78 47.78 0 016 13.12M12.265 3.11a.375.375 0 11-.53 0L12 2.845l.265.265zm-3 0a.375.375 0 11-.53 0L9 2.845l.265.265zm6 0a.375.375 0 11-.53 0L15 2.845l.265.265z"
                    />
                  </svg>
                </span>
              </div>
              <div className="flex justify-end mt-2 ">
                <span className="px-1">iran:qom</span>

                {/* map ip */}
                <span>
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 24 24"
                    stroke-width="1.5"
                    stroke="currentColor"
                    class="w-6 h-6"
                  >
                    <path
                      stroke-linecap="round"
                      stroke-linejoin="round"
                      d="M15 10.5a3 3 0 11-6 0 3 3 0 016 0z"
                    />
                    <path
                      stroke-linecap="round"
                      stroke-linejoin="round"
                      d="M19.5 10.5c0 7.142-7.5 11.25-7.5 11.25S4.5 17.642 4.5 10.5a7.5 7.5 0 1115 0z"
                    />
                  </svg>
                </span>

                <span className="px-2">jamesskidmore.com</span>

                {/* gmail */}
                <span>
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 24 24"
                    stroke-width="1.5"
                    stroke="currentColor"
                    class="w-6 h-6"
                  >
                    <path
                      stroke-linecap="round"
                      d="M16.5 12a4.5 4.5 0 11-9 0 4.5 4.5 0 019 0zm0 0c0 1.657 1.007 3 2.25 3S21 13.657 21 12a9 9 0 10-2.636 6.364M16.5 12V8.25"
                    />
                  </svg>
                </span>
              </div>
            </div>
          </div>
          <div className="basis-1/4 ">
            <div className="flex justify-start mx-2 mt-2 ">
              <Image
                src={"/profile/front_face.png"}
                objectFit={"cover"}
                width={128}
                height={128}
                alt={"پروفایل"}
                className={"rounded-3"}
              />
            </div>
          </div>
        </div>
        <div class="grid grid-rows-3 grid-flow-col gap-4">
          <div class="row-span-2 col-span-2 ...">03</div>
          <div class="row-span-3 ">01</div>
        </div>
      </div>
    </>
  );
};

export default Profile;

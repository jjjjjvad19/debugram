import { setCookie } from "cookies-next";
import Image from "next/image";
import Link from "next/link";
import { useDispatch } from "react-redux";
import { toast } from "react-toastify";
import { login } from "../../StoreManager/Reducers/userSlice";
import Button from "../../tools/elemnts/buttons/button";
import TextBox from "../../tools/elemnts/text/textBox";
import { GenericService } from "../../tools/service/genericService";
import useRouter from "next/router";

const LoginComponent = () => {
  const dispatch = useDispatch();
  const submitedLogin = async (event) => {
    event.preventDefault();
    var inputs = event.currentTarget.getElementsByTagName("input");
    const router = useRouter;
    if (inputs) {
      Array(...inputs).forEach((item) => {
        var valid = item.getAttribute("db-valid");
        if (valid && JSON.parse(valid)) {
          toast.error(item.getAttribute("db-validmessage"));
        }
      });
      if (
        Array(...inputs).some((n) => {
          if (JSON.parse(n.getAttribute("db-valid"))) return true;
        })
      )
        return false;
    }
    const url = process.env.HostName + process.env.LoginUser;
    var param = {
      Email: email.value,
      Password: password.value,
    };
    const data = await GenericService(url, param);
    if (data.isSuccess == true) {
      dispatch(login(data.data));
      setCookie("token", data.data.token);
      router.push("/", null);
    }
  };

  return (
    <>
      <div className="flex items-start min-h-screen py-16 lg:justify-center">
        <div className="flex flex-col overflow-hidden bg-back-4 rounded-md shadow-lg max md:flex-row md:flex-1 lg:max-w-screen-md">
          <div className=" text-white bg-blue-500 md:w-80 md:flex-shrink-0 md:flex md:flex-col md:items-center md:justify-evenly">
            <Image
              src="/Images/test.webp"
              alt="Picture of the author"
              width={600}
              height={1000}
            />
          </div>
          <div className="p-5 bg-white md:flex-1">
            <h3 className="my-4 text-2xl font-semibold text-gray-700">
              لاگین اکانت
            </h3>
            <form onSubmit={submitedLogin} className="flex flex-col space-y-5">
              <div className="space-y-6 mt-4">
                <TextBox
                  id={"email"}
                  name={"email"}
                  innerText={"ایمیل"}
                  isValid={true}
                  msgValidate={"ایمیل نمیتواند خالی باشد"}
                  Regular={/\w\@([a-z]{1,}|[A-Z]{1,})\.com$/g}
                  RxgMessage={"ایمیل وارد شده اشتباه میباشد"}
                />
                <TextBox
                  id={"password"}
                  name={"password"}
                  innerText={"رمز عبور"}
                />
              </div>
              <div>
                <button
                  type="submit"
                  className="w-100% px-4  py-2 rounded-2 text-sm font-semibold text-white transition-colors border border-border-0
                  bg-back-0
                   text-color-1 duration-300  rounded-md shadow  focus:outline-none focus:ring-4"
                >
                  ورود
                </button>
              </div>
              <div className="w-100 text-center text-sm">
                <span>Login With</span>
              </div>
              <div className="flex flex-col space-y-5">
                <Button
                  className="border border-border-2 h-10 rounded-2 
                  before:bg-back-3
                  before:absolute
                  before:w-100%
                  before:h-100%
                  before:opacity-50
                  before:z-0"
                  type={"button"}
                  icon={"url('/images/google.svg')"}
                  positionIcon={"center"}
                />
              </div>
            </form>
            <div className="py-4 px-3 w-100%  mt-5">
              <p className="text-xs">
                حسابی ندارید ؟
                <Link href={"/user/register"}>
                  <a className="text-sm text-color-2 underline underline-offset-4">
                    {" "}
                    ایجاد حساب جدید{" "}
                  </a>
                </Link>
              </p>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default LoginComponent;

import { toast } from "react-toastify";
import Button from "../../tools/elemnts/buttons/button";
import TextBox from "../../tools/elemnts/text/textBox";
import { GenericService } from "../../tools/service/genericService";
import useRouter from "next/router";
import { useDispatch } from "react-redux";
import { login } from "../../StoreManager/Reducers/userSlice";
import { setCookie } from "cookies-next";

const RegisterComponent = () => {
  const router = useRouter;
  const dispatch = useDispatch();

  const submitRegister = async (event) => {
    event.preventDefault();
    const param = {
      Email: email.value,
      FullName: fullName.value,
      Password: password.value,
      ConfirmPassword: confirmPassword.value,
    };
    var inputs = event.currentTarget.getElementsByTagName("input");
    if (inputs) {
      Array(...inputs).forEach((item) => {
        var valid = item.getAttribute("db-valid");
        if (valid && JSON.parse(valid)) {
          toast.error(item.getAttribute("db-validmessage"));
        }
      });
      if (
        Array(...inputs).some((n) => {
          if (JSON.parse(n.getAttribute("db-valid"))) return true;
        })
      )
        return false;
    }
    if (param.ConfirmPassword != param.Password)
      toast.error("رمز عبور با تکرار رمز عبور مطابقت ندارد");
    const url = process.env.HostName + process.env.RegisterUser;
    const data = await GenericService(url, param);
    debugger;
    if (data.isSuccess == true) {
      dispatch(login(data.data));
      setCookie("token", data.data.token);
      router.push("/", null);
    }
  };
  return (
    <>
      <div className="flex w-full min-h-screen  justify-center items-start ">
        <div
          className="border border-border-2 mt-16 bg-back-4 rounded-xl  px-8 py-4 text-gray-600 md:w-96"
          dir="rtl"
        >
          <form className="flex flex-col" onSubmit={submitRegister}>
            <div className="flex flex-col items-center justify-center gap-3">
              <p className=" ">دیباگرم</p>
              <p className="font-bold text-lg text-black">فرم ثبت نام</p>
            </div>

            <div className="space-y-6 mt-4">
              <TextBox
                id={"email"}
                name={"email"}
                innerText={"ایمیل"}
                isValid={true}
                msgValidate={"ایمیل نمیتواند خالی باشد"}
                Regular={/\w\@([a-z]{1,}|[A-Z]{1,})\.com$/g}
                RxgMessage={"ایمیل وارد شده اشتباه میباشد"}
              />
              <TextBox
                id={"fullName"}
                name={"fullName"}
                innerText={"نام و نام خانودگی"}
                isValid={true}
                msgValidate={"نام و نام خانوادگی نمیتواند خالی باشد"}
              />
              <TextBox
                id={"password"}
                name={"password"}
                innerText={"رمز عبور"}
                type="password"
                isValid={true}
              />
              <TextBox
                id={"confirmPassword"}
                name={"confirmPassword"}
                innerText={"تکرار رمز عبور"}
                isValid={true}
                type="password"
              />
            </div>
            <Button
              innerText={"ثبت نام"}
              type={"submit"}
              className={"h-10 bg-back-0 mt-4 rounded-2 text-color-1"}
            />
          </form>
          <div className="text-center mt-3 text-sm"></div>
        </div>
      </div>
    </>
  );
};

export default RegisterComponent;

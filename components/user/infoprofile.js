import { setCookie } from "cookies-next";
import Image from "next/image";
import Link from "next/link";
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { logOut } from "../../StoreManager/Reducers/userSlice";
const InfoProfile = () => {
  const [showMenuInfo, setShowMenuInfo] = useState(false);
  const dispatch = useDispatch();
  const user = useSelector((state) => state.reducerUser).user;
  return (
    <>
      {user == null ? (
        <div className="w-100% h-100% flex justify-end items-center px-1">
          <Link href={"/user/login"}>
            <a className="bg-back-1 h-8 py-2  border border-border- px-3 text-color-1 font-bold text-xs rounded-5">
              ورود / ثبت نام
            </a>
          </Link>
        </div>
      ) : (
        <div
          className="flex justify-end 
        items-center "
        >
          <div
            className="flex justify-center relative
        items-center md:bg-back-0 xs:bg-none px-4  rounded-10 cursor-pointer "
          >
            <h3
              className="ml-4 text-color-1 text-sm xs:hidden md:block select-none"
              onClick={() => {
                setShowMenuInfo(true);
              }}
            >
              {user.fullName}
            </h3>

            <div
              className="w-9 h-9 rounded-50% overflow-hidden
          border border-border-0 
          relative flex justify-center items-center bg-back-2 "
            >
              <Image
                src={`/Images/Avatar.png`}
                layout={"fixed"}
                width={30}
                height={30}
                alt={"آواتار"}
                onClick={() => {
                  setShowMenuInfo(true);
                }}
              />
            </div>
            <div
              className={`md:absolute xs:fixed md:left-0  md:top-0 xs:left-2 xs:top-2 ${
                showMenuInfo == true ? "block" : "hidden"
              } md:w-140% xs:w-56
               top-0 bg-back-0 py-2 px-1 z-50 rounded-3`}
            >
              <div className="w-100% flex justify-center items-center">
                <div className="flex justify-center items-center bg-back-4 px-3 py-1 rounded-10 ">
                  <h3 className="ml-4 text-sm">{user.fullName}</h3>

                  <div
                    className="w-9 h-9 rounded-50% overflow-hidden
border border-border-0 
relative flex justify-center items-center bg-back-2"
                  >
                    <Image
                      src={`/Images/Avatar.png`}
                      layout={"fixed"}
                      width={33}
                      height={34}
                      alt={"آواتار"}
                    />
                  </div>
                </div>

                <svg
                  onClick={() => setShowMenuInfo(false)}
                  xmlns="http://www.w3.org/2000/svg"
                  fill="none"
                  viewBox="0 0 24 24"
                  strokeWidth={1.5}
                  stroke="currentColor"
                  className="w-7 h-7 text-color-1"
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    d="M6 18L18 6M6 6l12 12"
                  />
                </svg>
              </div>
              <ul className="flex justify-center items-center flex-col text-sm text-color-1">
                <li className="my-1 w-100% border-b border-border-3 text-center py-2">
                  <Link href={"/profile"}>
                    <a className="w-100%">
                      <h6>پروفایل </h6>
                    </a>
                  </Link>
                </li>
                <li className="my-1 w-100% border-b border-border-3 text-center py-2">
                  <Link href={"/favorite"}>
                    <a className="w-100">
                      <h6>مطالب مورد علاقه</h6>
                    </a>
                  </Link>
                </li>
                <li className="my-1 w-100% text-center py-2">
                  <button
                    className="w-100%"
                    onClick={() => {
                      dispatch(logOut());
                      setCookie("token", "");
                    }}
                  >
                    خروج
                  </button>
                </li>
              </ul>
            </div>
          </div>
        </div>
      )}
    </>
  );
};

export default InfoProfile;

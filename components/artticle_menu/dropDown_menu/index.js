import React, { useEffect } from "react";

const DropDownMenu = () => {
  useEffect(() => {
    var ParentArticle = document.getElementsByClassName("parent-artticle");
    var parentChild = document.getElementsByClassName("parent-child-artticle");
    var i;
    for (i = 0; i < parentChild.length; i++) {
      parentChild[i].addEventListener("click", toggleItem, true);
    }
    function toggleItem() {
      var itemClass = this.parentNode.className;
      for (i = 0; i < ParentArticle.length; i++) {
        ParentArticle[i].className = "parent-artticle close";
      }
      if (itemClass == "parent-artticle close") {
        this.parentNode.className = "parent-artticle open";
      }
    }
  }, []);
  return (
    <>
      <ul className="accordionWrapper  h-[47rem]">
        {Array(100)
          .fill(2)
          .map((index) => {
            return (
              <li className="px-2 pt-4" key={index}>
                <div className="parent-artticle close">
                  <div className="relative parent-child-artticle">
                    <span className="absolute pt-1">
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        fill="none"
                        viewBox="0 0 24 24"
                        strokeWidth="1.5"
                        stroke="currentColor"
                        className="w-5 h-5"
                        id="svg"
                      >
                        <path
                          strokeLinecap="round"
                          strokeLinejoin="round"
                          d="M10.5 19.5L3 12m0 0l7.5-7.5M3 12h18"
                        />
                      </svg>
                    </span>
                    <h2 className="px-6 italic font-bold capitalize ">
                      اموزش جاوا اسکریپت
                    </h2>
                  </div>
                  <div className="accordionItemContent pt-3">
                    <div className="table w-100%  pr-6 ">
                      <p>مپ کردن</p>
                      <p className="pt-2">استرینگ</p>
                      <p className="pt-2">ارایه ها</p>
                    </div>
                  </div>
                </div>
              </li>
            );
          })}
      </ul>
    </>
  );
};

export default DropDownMenu;

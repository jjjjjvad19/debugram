import { useState } from "react";

const ThemeHeader = () => {
  const [isChangeTheme, setChangeTheme] = useState(false);
  const theme = require("../../public/theme.json");
  return (
    <>
      <div className="w-100% h-100% flex justify-end  items-center px-3 ">
        <div className="xl:w-40% h-100% flex justify-center items-center gap-2 py-4 flex-wrap p-1">
          {isChangeTheme ? (
            Object.getOwnPropertyNames(theme).map((n, index) => {
              {
                return (
                  <div
                    key={index}
                    className={`w-6 h-6 rounded-50% cursor-pointer`}
                    style={{ backgroundColor: n }}
                    onClick={() => {
                      Object.getOwnPropertyNames(theme[n]).map((p) => {
                        theme[n][p].map((x, index) => {
                          document.documentElement.style.setProperty(
                            `--${p}-${index}`,
                            x
                          );
                        });
                      });
                    }}
                  ></div>
                );
              }
            })
          ) : (
            <></>
          )}
        </div>
        <div
          onClick={() => setChangeTheme(!isChangeTheme)}
          className="xl:w-60% bg-back-0 h-9 rounded-5 p-1 px-2 text-sm relative overflow-hidden cursor-pointer
         text-color-1 gap-x-2 flex justify-center  items-center"
        >
          <span className="select-none">Theme</span>
          <svg
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 24 24"
            strokeWidth={1.5}
            stroke="currentColor"
            className="w-7 h-7 text-color-4"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              d="M12 3v2.25m6.364.386l-1.591 1.591M21 12h-2.25m-.386 6.364l-1.591-1.591M12 18.75V21m-4.773-4.227l-1.591 1.591M5.25 12H3m4.227-4.773L5.636 5.636M15.75 12a3.75 3.75 0 11-7.5 0 3.75 3.75 0 017.5 0z"
            />
          </svg>
        </div>
      </div>
    </>
  );
};

export default ThemeHeader;

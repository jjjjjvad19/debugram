import { useState } from "react";

const ThemeMenu = () => {
  const [isChangeTheme, setChangeTheme] = useState(false);
  const theme = require("../../public/theme.json");

  return (
    <>
      <div className="w-100% h-100% flex justify-start  items-start py-3">
        <div
          onClick={() => setChangeTheme(!isChangeTheme)}
          className="w-50% h-11 rounded-5 relative overflow-hidden cursor-pointer
      before:bg-back-3 before:opacity-30 before:-z-20 before:absolute
      before:w-100% before:h-100%
      text-color-0 gap-x-2 flex justify-center  items-center"
        >
          <span className="select-none">Theme</span>
          <svg
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 24 24"
            strokeWidth={1.5}
            stroke="currentColor"
            className="w-7 h-7 text-color-4"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              d="M12 3v2.25m6.364.386l-1.591 1.591M21 12h-2.25m-.386 6.364l-1.591-1.591M12 18.75V21m-4.773-4.227l-1.591 1.591M5.25 12H3m4.227-4.773L5.636 5.636M15.75 12a3.75 3.75 0 11-7.5 0 3.75 3.75 0 017.5 0z"
            />
          </svg>
        </div>
        <div className="w-50% h-100% flex justify-center items-center gap-2 flex-wrap p-1">
          {isChangeTheme ? (
            Object.getOwnPropertyNames(theme).map((n, index) => {
              {
                return (
                  <div
                    key={index}
                    className={`w-10 h-10 rounded-50% cursor-pointer`}
                    style={{ backgroundColor: n }}
                    onClick={() => {
                      Object.getOwnPropertyNames(theme[n]).map((p) => {
                        theme[n][p].map((x, index) => {
                          document.documentElement.style.setProperty(
                            `--${p}-${index}`,
                            x
                          );
                        });
                      });
                    }}
                  ></div>
                );
              }
            })
          ) : (
            <></>
          )}
        </div>
      </div>
    </>
  );
};

export default ThemeMenu;

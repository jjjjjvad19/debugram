const LableMenu = ({ title }) => {
  return (
    <li className="h-10 relative flex xs:w-100% justify-start px-3 items-center xs:justify-center overflow-hidden cursor-pointer">
      {title}
    </li>
  );
};

export default LableMenu;

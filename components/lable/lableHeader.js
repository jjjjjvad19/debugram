const LableHeader = ({ title }) => {
  return (
    <li
      className="h-100% relative flex justify-center items-center  overflow-hidden cursor-pointer
      before:absolute 
      before:h-1
      before:w-100%
      before:top-95%
      before:translate-x-100%
      before:bg-border-1
      before:transition-all
      hover:before:translate-x-50%            
      after:absolute 
      after:h-1
      after:w-100%
      after:top-95%
      after:-translate-x-100%
      after:bg-border-1
      after:transition-all
      hover:after:-translate-x-50%"
    >
      {title}
    </li>
  );
};

export default LableHeader;

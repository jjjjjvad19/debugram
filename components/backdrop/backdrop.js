const BackDrop = () => {
  return (
    <div
      className="
       fixed
        z-30
        w-100%
       h-100%
        backdrop-blur-sm
       inset-0"
    ></div>
  );
};

export default BackDrop;

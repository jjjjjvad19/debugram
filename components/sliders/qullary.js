import Image from "next/image";
const gullary = () => {
  return (
    <>
      <div>
        <div className="w-90% mx-auto space-y-2 lg:space-y-0 lg:gap-2 lg:grid lg:grid-cols-4 my-7  overflow-hidden h-auto ">
          {Array(8)
            .fill(1)
            .map((n, index) => (
              <div
                className="relative w-100% rounded-5 opacity-1 overflow-hidden group cursor-pointer "
                key={index}
              >
                <Image
                  objectFit="cover"
                  alt={"اسلایدر"}
                  width={400}
                  height={200}
                  src={"/slidergullary/CAZ6JXi6huSuN4QGE627NR.jpg"}
                  className="group-hover:filter group-hover:brightness-50"
                />
                <div
                  className="absolute translate-y-100% transition-all duration-300 delay-200 bottom-0 left-0 right-0 px-4 py-2 bg-back-1 group-hover:translate-y-0  opacity-70"
                  id="testing"
                >
                  <h3 className="text-xl text-white font-bold">
                    Hey, I Am The Big Boss
                  </h3>
                  <p className="mt-2 text-sm ">
                    Some description text. Some dummy text here. Welcome to
                    KindaCode.com
                  </p>
                </div>
              </div>
            ))}
        </div>
        <p className="text-center text-xl p-4 font-bold ">بیشتر </p>
      </div>
    </>
  );
};

export default gullary;

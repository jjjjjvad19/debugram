import Slider from "react-slick";
import Image from "next/image";
const Index = () => {
  var settings = {
    dots: false,
    infinite: true,
    speed: 1500,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 3000,
    lazyLoad: true,
    arrows: false,
    slickGoTo: 2,
    accessibility: true,
    arrows: true,
  };
  return (
    <>
      <div className="relative">
        <div className=" flex justify-center items-center absolute bottom-0 w-100%   ">
          <div className="h-16 relative w-25%  first-letter:xs:w-50% xs:w-60% sm:w-50% ">
            <input
              type="search"
              className="absolute w-100% h-95% rounded-5 top-[34%]  z-10  outline-none rounded
                transition ease-in-out
                focus:text-colors-2 focus:bg-white focus:border-border-1 focus:outline-none"
              placeholder="جستجو کنید"
            />
          </div>
        </div>
        <Slider {...settings} dir="ltr">
          {Array(4)
            .fill(1)
            .map((n, index) => (
              <div key={index} className="w-100% h-72 relative">
                <Image
                  objectFit="fill"
                  layout="fill"
                  alt={"اسلایدر"}
                  width={100}
                  height={100}
                  src={
                    "/slider/087f9232769edae07fad6e618621782df6b84826_1665496888.jpg"
                  }
                  className="block w-100% h-100%"
                />
              </div>
            ))}
        </Slider>
      </div>
    </>
  );
};

export default Index;

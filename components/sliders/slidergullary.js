import React from "react";
import Slider from "react-slick";
import Image from "next/image";
const Slidergullary = () => {
  var settings = {
    dots: false,
    infinite: true,
    speed: 1500,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 9000,
    arrows: false,
  };

  return (
    <>
      <div dir="ltr">
        <p className="text-end  xs:px-10 p-2 lg:px-20 text-sm font-bold sm:px-10 ">
          جدید ترین
        </p>
        <Slider {...settings} dir="ltr">
          {Array(4)
            .fill(1)
            .map((n, index) => (
              <div
                className="w-90% mx-auto grid grid-cols-2 gap-2 lg:gap-2  lg:grid lg:grid-cols-4 lg:space-y-2 lg:my-7"
                key={index}
              >
                {Array(4)
                  .fill(1)
                  .map((n, index) => (
                    <div
                      className="relative w-100% rounded-5 opacity-1 overflow-hidden  cursor-pointer "
                      key={index}
                    >
                      <Image
                        objectFit="fill"
                        src="/slidergullary/new.jpg"
                        alt={"اسلایدر گالری"}
                        width={300}
                        height={200}
                      />
                    </div>
                  ))}
              </div>
            ))}
        </Slider>
      </div>
    </>
  );
};

export default Slidergullary;

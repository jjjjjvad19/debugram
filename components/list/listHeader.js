const ListHeader = () => {
  return (
    <>
      <ol
        className="mt-2 absolute top-100%  invisible  flex-col gap-y-2 
       bg-back-0 justify-center items-center  rounded-1 flex
       transition-all   group-hover:visible z-50  group-hover:h-auto duration-150"
      >
        <li className="text-xs text-color-1  py-2 px-4 text-start whitespace-nowrap hover:bg-back-1 w-100% ">
          Sql Server Sql Server Sql Server
        </li>
        <li className="text-xs text-color-1  py-2 px-4 text-start whitespace-nowrap hover:bg-back-1 w-100% ">
          Sql Server Sql Server Sql Server
        </li>
        <li className="text-xs text-color-1  py-2 px-4 text-start whitespace-nowrap hover:bg-back-1 w-100% ">
          Sql Server Sql Server Sql Server
        </li>
        <li className="text-xs text-color-1  py-2 px-4 text-start whitespace-nowrap hover:bg-back-1 w-100% ">
          Sql Server Sql Server Sql Server
        </li>
        <li className="text-xs text-color-1  py-2 px-4 text-start whitespace-nowrap hover:bg-back-1 w-100% ">
          Sql Server Sql Server Sql Server
        </li>
      </ol>
    </>
  );
};

export default ListHeader;

import Image from "next/image";
import Link from "next/link";
import { useState } from "react";
import BackDrop from "../backdrop/backdrop";
import LableHeader from "../lable/lableHeader";
import LableMenu from "../lable/lableMenu";
import ListHeader from "../list/listHeader";
import InfoProfile from "../user/infoprofile";
import ThemeHeader from "../Themes/themeHeader";
import ThemeMenu from "../Themes/themeMenu";

const HeaderPage = () => {
  const [showTools, setShowTool] = useState(false);
  return (
    <>
      <header
        className="
         w-100%
         h-32
         "
      >
        {showTools ? (
          <>
            <BackDrop />
            <div
              className="md:hidden fixed  xs:w-72 w-60% mr-5 top-3 shadow-inner rounded-1 border border-border-4 overflow-hidden 
           bg-back-0 z-50 text-color-1"
            >
              <div className="w-20 z-50 absolute left-0 h-10 px-4 flex justify-end items-center">
                <svg
                  onClick={() => setShowTool(false)}
                  xmlns="http://www.w3.org/2000/svg"
                  fill="none"
                  viewBox="0 0 24 24"
                  strokeWidth={1.5}
                  stroke="currentColor"
                  className="w-7 h-7"
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    d="M6 18L18 6M6 6l12 12"
                  />
                </svg>
              </div>
              <ul className="p-0 h-100% w-100%  justify-start items-start py-4 flex   gap-y-1 flex-col border-b border-border-2">
                <LableMenu title={"JavaScript"} />
                <LableMenu title={"Sql Server"} />
                <LableMenu title={"C#"} />
                <LableMenu title={"Css3"} />
                <LableMenu title={"Html5"} />
              </ul>
              <div className="w-100% px-3">
                <ThemeMenu />
              </div>
            </div>
          </>
        ) : (
          <></>
        )}
        <div
          className=" 
         relative
         w-100%
         flex
         justify-center
         items-center
         lg:px-40
         md:px-16
         sm:px-0
         md:h-30%
         xs:h-50%
         before:absolute
         before:-z-20
         before:w-100%
         before:h-100%
         before:opacity-30
         before:bg-back-0
         before:shadow-inner
         before:backdrop-blur-sm
         before:blur-2xl"
        >
          <div className="w-100% h-100% py-1 xs:flex xs:justify-center xs:items-center md:block">
            <div className="flex justify-start items-center md:w-75% xs:w-10%  h-100% float-right md:border-l md:border-border-4  ">
              <ul className="p-0 h-100% w-100%  justify-start items-center md:flex xs:hidden gap-x-4 flex-wrap">
                <LableHeader title={"Js"} />
                <LableHeader title={"C#"} />
                <LableHeader title={"Html5"} />
                <LableHeader title={"Css"} />
                <LableHeader title={"Sql Server"} />
                <LableHeader title={"Angular"} />
                <LableHeader title={"React"} />
                <LableHeader title={"NextJs"} />
              </ul>
              <div
                className="flex justify-center items-center p-2 md:hidden cursor-pointer"
                onClick={() => setShowTool(true)}
              >
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  fill="none"
                  viewBox="0 0 24 24"
                  strokeWidth={1.5}
                  stroke="currentColor"
                  className="w-6 h-6"
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    d="M3.75 6.75h16.5M3.75 12h16.5m-16.5 5.25h16.5"
                  />
                </svg>
              </div>
            </div>
            <div className="xs:w-50% md:hidden h-100% flex justify-end items-center">
              <Link href={"/"}>
                <a className="h-100% ml-5">
                  <Image
                    src={"/Images/db.png"}
                    objectFit={"cover"}
                    width={50}
                    height={50}
                    alt={"لوگو"}
                    className={"rounded-50%"}
                  />
                </a>
              </Link>
            </div>
            <div className="xs:w-40% md:w-25% h-100% float-left md:block xs:flex xs:justify-end xs:items-center">
              <InfoProfile />
            </div>
          </div>
        </div>
        <div
          className=" 
         xs:hidden
         md:flex
         relative
         w-100%
         h-70%
         lg:px-16
         xl:px-16
          justify-center items-center
         before:absolute
         before:w-100%
         before:h-100%
         before:-z-10
         before:opacity-5
         before:bg-back-1
         before:shadow-sm
         before:backdrop-blur-sm
         border-b border-b-border-4"
        >
          <div className="float-right flex justify-start items-center xl:w-60% lg:w-80% h-100%">
            <div className="flex justify-start gap-x-4 items-center xl:w-75%  lg:w-65% h-100%">
              <Link href={"/"}>
                <a className="md:hidden">
                  <Image
                    src={"/Images/db.png"}
                    objectFit={"cover"}
                    width={50}
                    height={50}
                    alt={"لوگو"}
                    className={"rounded-50%"}
                  />
                </a>
              </Link>
              <ul className="flex justify-start md:gap-x-4  xl:gap-x-8 items-center w-100% text-xs text-color-0 md:px-1 lg:px-3 h-100%">
                <li className="cursor-pointer">آموزش گرافیگ</li>
                <li className="cursor-pointer">آموزش گرافیگ</li>
                <li className="cursor-pointer">آموزش گرافیگ</li>
                <li className="cursor-pointer">آموزش گرافیگ</li>
                <li className="cursor-pointer flex items-center relative justify-center group">
                  <span className="ml-1">آموزش Sql Server</span>
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 24 24"
                    strokeWidth={1.5}
                    stroke="currentColor"
                    className="w-4 h-4"
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      d="M19.5 8.25l-7.5 7.5-7.5-7.5"
                    />
                  </svg>
                  <ListHeader />
                </li>
              </ul>
            </div>
            <div className="xl:w-25% lg:w-30% md:hidden lg:flex h-100%  justify-start items-center">
              <input
                type={"text"}
                placeholder={"جستجو کنید ..."}
                autoComplete={"off"}
                className={
                  "w-100% h-9 text-sm rounded-10  border border-border-3  outline-color-2  caret-color-0"
                }
              />
            </div>
          </div>
          <div className="float-left flex justify-end items-center xl:w-20% md:w-30%  h-100%">
            <div className="w-100% h-100% md:block xs:hidden">
              <ThemeHeader />
            </div>
          </div>
        </div>
      </header>
    </>
  );
};

export default HeaderPage;

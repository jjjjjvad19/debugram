const IconBadge = ({ children, badge, title }) => {
  if (badge >= 100) badge = "+99";
  return (
    <>
      <div
        title={title}
        badge={badge}
        className={`w-100% relative h-100% 
        cursor-pointer
      before:absolute before:top-70% 
      before:h-4
      ${badge ? "before:px-1" : ""}
      before:bg-[#e60f0fe0]
      before:left-0
      before:rounded-5
      before:text-color-1
      before:flex
      before:justify-center
      before:items-center
      before:text-xs
      before:content-[attr(badge)]`}
      >
        {children}
      </div>
    </>
  );
};

export default IconBadge;

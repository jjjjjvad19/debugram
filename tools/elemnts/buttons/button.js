const Button = ({ type, innerText, icon, className, positionIcon }) => {
  return (
    <>
      <div
        className={`w-100% flex justify-center items-center  relative ${
          className ? className : ""
        }`}
      >
        <button type={type} className={` w-100% z-20 h-100% absolute`}>
          {innerText}
        </button>
        <div
          className={`absolute w-100%  pt-2
          flex justify-${positionIcon} items-center  
          after:z-10
          icon`}
        ></div>
      </div>

      <style global jsx>{`
        .icon:after {
          content: ${icon} !important;
        }
      `}</style>
    </>
  );
};

export default Button;

import AwesomeDebouncePromise from "awesome-debounce-promise";
import { useState } from "react";
import { useAsync } from "react-async-hook";
import useConstant from "use-constant";

const Debounce = (functionParam) => {
  const [inputData, setInputData] = useState({ text: "", dispatch: null });

  const debounceFunction = useConstant(() =>
    AwesomeDebouncePromise(functionParam, 600)
  );

  const functionResult = useAsync(async () => {
    if (inputData.text.length === 0) return [];
    else {
      return debounceFunction(inputData);
    }
  }, [debounceFunction, inputData.text, inputData.dispatch]);
  return {
    inputData,
    setInputData,
    functionResult,
  };
};

export default Debounce;

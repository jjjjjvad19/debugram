import { useState } from "react";

const TextBox = ({
  id,
  name,
  innerText,
  isValid = false,
  msgValidate,
  className,
  Regular,
  onChange,
  RxgMessage,
  type = "text",
}) => {
  const [validate, setValidate] = useState(0);
  const [text, setText] = useState(null);
  const change = (event) => {
    setText(event.currentTarget.value);
    if (isValid) {
      if (!event.currentTarget.value) {
        setValidate(3);
        return;
      } else setValidate(2);

      if (Regular) {
        const reg = new RegExp(Regular);
        if (!reg.exec(event.currentTarget.value)) {
          setValidate(1);
        } else {
          setValidate(2);
        }
        return;
      }
    }
    onChange && onChange(event);
  };
  return (
    <>
      <div className="relative">
        <input
          type={type}
          id={id}
          db-valid={`${
            (isValid && text == null) ||
            text == "" ||
            validate == 3 ||
            validate == 1
          }`}
          db-validmessage={`${
            RxgMessage && validate == 1
              ? RxgMessage
              : msgValidate
              ? msgValidate
              : `${innerText} نمیتواند خالی باشد`
          }`}
          className={`peer w-100% block px-2.5 rounded-2 pb-2.5 pt-2  
          text-sm  bg-transparent  border-1
               focus:outline-none focus:ring-0 
               focus:border-border-2 
               ${
                 validate == 0
                   ? ""
                   : validate == 1 || validate == 3
                   ? "border-[#dc3545]"
                   : "border-[#28a745]"
               } ${className ? className : ""}`}
          placeholder={" "}
          name={name}
          onChange={change}
        />
        <label
          htmlFor={id}
          className={`absolute w-auto 
                     duration-75 transition-all transform -translate-y-4 scale-95  z-10 px-2 top-1 before:backdrop-blur-xl
                     peer-focus:px-2 
                     peer-placeholder-shown:scale-100 peer-placeholder-shown:-translate-y-6 peer-placeholder-shown:top-1/2
                     peer-indeterminate:bg-back-0
                     peer-focus:top-1
                     peer-focus:scale-[0.80] peer-focus:-translate-y-4 right-2
                     before:absolute
                     before:w-100%
                     before:h-60%
                     peer-focus:before:backdrop-blur-xl
                     peer-placeholder-shown:before:before:backdrop-blur-0             
                     before:-z-50`}
        >
          {innerText}
        </label>
        <p
          className={`text-[#dc3545] text-sm ${
            validate == 3 || validate == 1 ? "visible" : "invisible"
          }`}
        >
          {RxgMessage && validate == 1
            ? RxgMessage
            : msgValidate
            ? msgValidate
            : `${innerText} نمیتواند خالی باشد`}
        </p>
      </div>
    </>
  );
};

export default TextBox;

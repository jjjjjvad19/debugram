import axios from "axios";
import { toast } from "react-toastify";
import { getCookie } from "cookies-next";

export const GenericService = async (
  url,
  param,
  query,
  token = null,
  method = "post"
) => {
  const id = toast.loading("...در حال بارگذاری ");
  if (token == null) token = getCookie("token");
  var result = null;
  await axios[method](url, param, {
    params: query,
    headers: { token: `${token}` },
  })
    .then((res) => {
      result = res;
      if (result.data.isSuccess == true)
        toast.update(id, {
          render: result.data.message,
          type: toast.TYPE.SUCCESS,
          isLoading: false,
          autoClose: 3500,
          draggableDirection: "x",
          draggable: true,
          draggablePercent: 100,
        });
      else
        toast.update(id, {
          render: result.data.message,
          type: toast.TYPE.ERROR,
          isLoading: false,
          autoClose: 3500,
          draggableDirection: "x",
          draggable: true,
          draggablePercent: 100,
        });
    })
    .catch((response) => {
      result = response;
      if (
        response.data &&
        response.data.errors &&
        response.data.errors.length != 0
      )
        response.data.errors.forEach((item) => {
          toast.update(id, {
            render: item,
            type: toast.TYPE.ERROR,
            isLoading: false,
            autoClose: 3500,
          });
        });
      else if (response.data && response.data.message) {
        toast.update(id, {
          render: response.data.message,
          type: toast.TYPE.ERROR,
          isLoading: false,
          autoClose: 3500,
        });
      } else {
        result.data = { isSuccess: false };
        toast.update(id, {
          render: "خطایی سمت سرور رخ داد",
          type: toast.TYPE.ERROR,
          isLoading: false,
          autoClose: 3500,
        });
      }
    });
  return result.data;
};

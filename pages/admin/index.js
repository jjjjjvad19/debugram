import { PolarArea } from "react-chartjs-2";
import ChartA from "chart.js/auto";
import LayoutAdmin from "../../layouts/admin/adminLayout";
import WayURL from "../../components/utility/wayURL";
const Index = () => {
  return (
    <>
      <div className="w-100% h-100% px-2">
        <div className="w-100% h-10 flex justify-start items-center">
          <WayURL data={[{ text: "صفحه اصلی" }, { text: "داشبورد" }]} />
        </div>
        <div className="w-100% mx-auto bg-[#0000009f] rounded-t-10 rounded-b-2">
          <div className="w-100% gap-y-10 gap-x-8 px-8 justify-center flex items-center flex-wrap py-4">
            {/* ------------  items Information basic Site */}

            {/* ------------ For Users */}
            <div className="rounded-2 max-w-20% w-100% h-100% bg-[#eee] px-3 py-2">
              <div className="h-70% w-100% border-b border-[#00ABB3] flex justify-center items-center">
                <div className="w-30% h-100% flex justify-center items-center">
                  <div
                    className="w-12 h-12 
                              flex justify-center items-center
                               rounded-50% bg-gradient-to-r from-[#f8af04] to-[#f76222] shadow-xl shadow-[#cfcfcf]"
                  >
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      fill="none"
                      viewBox="0 0 24 24"
                      strokeWidth={1.5}
                      stroke="currentColor"
                      className="w-70% h-70% text-[#fff]"
                    >
                      <path
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        d="M17.982 18.725A7.488 7.488 0 0012 15.75a7.488 7.488 0 00-5.982 2.975m11.963 0a9 9 0 10-11.963 0m11.963 0A8.966 8.966 0 0112 21a8.966 8.966 0 01-5.982-2.275M15 9.75a3 3 0 11-6 0 3 3 0 016 0z"
                      />
                    </svg>
                  </div>
                </div>
                <div className="w-70% h-100% flex justify-center items-end gap-y-2 flex-col">
                  <h3 className="text-lg">User</h3>
                  <p className="text-sm"> Users Online : 20000</p>
                </div>
              </div>
              <div className="w-100% h-30% text-end py-2">
                <p className="text-xs"> All Users : 3443434</p>
              </div>
            </div>

            {/* ------------ For Article */}
            <div className="rounded-2 max-w-20% w-100% h-100% bg-[#eee] px-3 py-2">
              <div className="h-70% w-100% border-b border-[#00ABB3] flex justify-center items-center">
                <div className="w-30% h-100% flex justify-center items-center">
                  <div
                    className="w-12 h-12 
                              flex justify-center items-center
                               rounded-50% bg-gradient-to-r from-[#51ff00] to-[#09ff00] shadow-xl shadow-[#cfcfcf]"
                  >
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      fill="none"
                      viewBox="0 0 24 24"
                      strokeWidth={1.5}
                      stroke="currentColor"
                      className="w-6 h-6 text-[#747474]"
                    >
                      <path
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        d="M15.75 17.25v3.375c0 .621-.504 1.125-1.125 1.125h-9.75a1.125 1.125 0 01-1.125-1.125V7.875c0-.621.504-1.125 1.125-1.125H6.75a9.06 9.06 0 011.5.124m7.5 10.376h3.375c.621 0 1.125-.504 1.125-1.125V11.25c0-4.46-3.243-8.161-7.5-8.876a9.06 9.06 0 00-1.5-.124H9.375c-.621 0-1.125.504-1.125 1.125v3.5m7.5 10.375H9.375a1.125 1.125 0 01-1.125-1.125v-9.25m12 6.625v-1.875a3.375 3.375 0 00-3.375-3.375h-1.5a1.125 1.125 0 01-1.125-1.125v-1.5a3.375 3.375 0 00-3.375-3.375H9.75"
                      />
                    </svg>
                  </div>
                </div>
                <div className="w-70% h-100% flex justify-center items-end gap-y-2 flex-col">
                  <h3 className="text-lg">Articles</h3>
                  <p className="text-sm"> Some Articles : 3000</p>
                </div>
              </div>
              <div className="w-100% h-30% text-end py-2">
                <p className="text-xs">Categories of articles : 20</p>
              </div>
            </div>

            {/* ------------ For Bugs */}
            <div className="rounded-2 max-w-20% w-100% h-100% bg-[#eee] px-3 py-2">
              <div className="h-70% w-100% border-b border-[#00ABB3] flex justify-center items-center">
                <div className="w-30% h-100% flex justify-center items-center">
                  <div
                    className="w-12 h-12 
                              flex justify-center items-center
                               rounded-50% bg-gradient-to-r from-[#f80404] to-[#f74d22] shadow-xl shadow-[#cfcfcf]"
                  >
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      fill="none"
                      viewBox="0 0 24 24"
                      strokeWidth={1.5}
                      stroke="currentColor"
                      className="w-6 h-6 text-[#fff]"
                    >
                      <path
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        d="M12 12.75c1.148 0 2.278.08 3.383.237 1.037.146 1.866.966 1.866 2.013 0 3.728-2.35 6.75-5.25 6.75S6.75 18.728 6.75 15c0-1.046.83-1.867 1.866-2.013A24.204 24.204 0 0112 12.75zm0 0c2.883 0 5.647.508 8.207 1.44a23.91 23.91 0 01-1.152 6.06M12 12.75c-2.883 0-5.647.508-8.208 1.44.125 2.104.52 4.136 1.153 6.06M12 12.75a2.25 2.25 0 002.248-2.354M12 12.75a2.25 2.25 0 01-2.248-2.354M12 8.25c.995 0 1.971-.08 2.922-.236.403-.066.74-.358.795-.762a3.778 3.778 0 00-.399-2.25M12 8.25c-.995 0-1.97-.08-2.922-.236-.402-.066-.74-.358-.795-.762a3.734 3.734 0 01.4-2.253M12 8.25a2.25 2.25 0 00-2.248 2.146M12 8.25a2.25 2.25 0 012.248 2.146M8.683 5a6.032 6.032 0 01-1.155-1.002c.07-.63.27-1.222.574-1.747m.581 2.749A3.75 3.75 0 0115.318 5m0 0c.427-.283.815-.62 1.155-.999a4.471 4.471 0 00-.575-1.752M4.921 6a24.048 24.048 0 00-.392 3.314c1.668.546 3.416.914 5.223 1.082M19.08 6c.205 1.08.337 2.187.392 3.314a23.882 23.882 0 01-5.223 1.082"
                      />
                    </svg>
                  </div>
                </div>
                <div className="w-70% h-100% flex justify-center items-end gap-y-2 flex-col">
                  <h3 className="text-lg">Bugs</h3>
                  <p className="text-sm">Some Bugs : 30000</p>
                </div>
              </div>
              <div className="w-100% h-30% flex justify-beetwen items-center py-2">
                <p className="text-xs w-50%">warning : 100</p>
                <p className="text-xs text-end w-50%">error : 4000</p>
              </div>
            </div>

            <div className="rounded-2 max-w-20% w-100% h-100% bg-[#eee] px-3 py-2">
              <div className="h-70% w-100% border-b border-[#00ABB3] flex justify-center items-center">
                <div className="w-30% h-100% flex justify-center items-center">
                  <div
                    className="w-12 h-12 
                              flex justify-center items-center
                               rounded-50% bg-gradient-to-r from-[#f80404] to-[#f74d22] shadow-xl shadow-[#cfcfcf]"
                  >
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      fill="none"
                      viewBox="0 0 24 24"
                      strokeWidth={1.5}
                      stroke="currentColor"
                      className="w-6 h-6 text-[#fff]"
                    >
                      <path
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        d="M12 12.75c1.148 0 2.278.08 3.383.237 1.037.146 1.866.966 1.866 2.013 0 3.728-2.35 6.75-5.25 6.75S6.75 18.728 6.75 15c0-1.046.83-1.867 1.866-2.013A24.204 24.204 0 0112 12.75zm0 0c2.883 0 5.647.508 8.207 1.44a23.91 23.91 0 01-1.152 6.06M12 12.75c-2.883 0-5.647.508-8.208 1.44.125 2.104.52 4.136 1.153 6.06M12 12.75a2.25 2.25 0 002.248-2.354M12 12.75a2.25 2.25 0 01-2.248-2.354M12 8.25c.995 0 1.971-.08 2.922-.236.403-.066.74-.358.795-.762a3.778 3.778 0 00-.399-2.25M12 8.25c-.995 0-1.97-.08-2.922-.236-.402-.066-.74-.358-.795-.762a3.734 3.734 0 01.4-2.253M12 8.25a2.25 2.25 0 00-2.248 2.146M12 8.25a2.25 2.25 0 012.248 2.146M8.683 5a6.032 6.032 0 01-1.155-1.002c.07-.63.27-1.222.574-1.747m.581 2.749A3.75 3.75 0 0115.318 5m0 0c.427-.283.815-.62 1.155-.999a4.471 4.471 0 00-.575-1.752M4.921 6a24.048 24.048 0 00-.392 3.314c1.668.546 3.416.914 5.223 1.082M19.08 6c.205 1.08.337 2.187.392 3.314a23.882 23.882 0 01-5.223 1.082"
                      />
                    </svg>
                  </div>
                </div>
                <div className="w-70% h-100% flex justify-center items-end gap-y-2 flex-col">
                  <h3 className="text-lg">Bugs</h3>
                  <p className="text-sm">Some Bugs : 30000</p>
                </div>
              </div>
              <div className="w-100% h-30% flex justify-beetwen items-center py-2">
                <p className="text-xs w-50%">warning : 100</p>
                <p className="text-xs text-end w-50%">error : 4000</p>
              </div>
            </div>

            {/* ------------ end Items Information basic Site */}
          </div>
        </div>
        <div className="w-50% py-10">
          <PolarArea
            data={{
              labels: ["User", "Article", "Bugs"],
              datasets: [
                {
                  id: 1,
                  label: "",
                  data: [10, 20, 1, 40, 50],
                },
                {
                  id: 2,
                  label: "",
                  data: [3, 2, 1],
                },
              ],
            }}
          />
        </div>
      </div>
    </>
  );
};

export default Index;

Index.Layout = function getLayout({ children }) {
  return <LayoutAdmin>{children}</LayoutAdmin>;
};

import LayoutPage from "../layouts/common/layoutPage";
import "../styles/globals.scss";
import "../public/font/font.css";
import "react-toastify/dist/ReactToastify.css";
import { ToastContainer } from "react-toastify";
import Router from "next/router";
import DeviceMode from "../components/device/deviceMode";
import { Provider } from "react-redux";
import store, { persistor } from "../StoreManager/store";
import { PersistGate } from "redux-persist/integration/react";

function MyApp({ Component, pageProps }) {
  Router.events.on("routeChangeComplete", (rq) => {
    window.scroll({
      behavior: "smooth",
      top: 0,
      left: 0,
    });
    __next.style.overflowY = "auto";
  });
  const Layout = Component.Layout || LayoutPage;
  return (
    <>
      <Provider store={store}>
        <PersistGate persistor={persistor}>
          <Layout>
            <DeviceMode></DeviceMode>
            <Component {...pageProps} />
          </Layout>
        </PersistGate>
      </Provider>
      <ToastContainer
        position="top-center"
        bodyStyle={{
          fontFamily: "IranYekan",
          fontSize: "13px",
          textAlign: "center",
        }}
      />
    </>
  );
}

export default MyApp;

import DropDownMenu from "../../components/artticle_menu/dropDown_menu";
import React, { useState, useEffect, useRef } from "react";
const Article = () => {
  const [scrollPosition, setScrollPosition] = useState(0);
  const [classElementFixed, setClassElementFixed] = useState("relative w-100%");
  const parentRef = useRef();
  const handleScroll = () => {
    const position = window.pageYOffset;
    setScrollPosition(position);
  };
  useEffect(() => {
    window.addEventListener("scroll", handleScroll, { passive: true });
    const parent = parentRef.current;
    const heightHeader = parent.offsetTop;
    if (scrollPosition >= heightHeader) setClassElementFixed("sticky  top-0");
    else setClassElementFixed("relative w-100%");
  }, [scrollPosition]);

  return (
    <div className="w-full flex flex-row mt-1" dir="ltr">
      <div className="relative basis-1/4 lg:basis-12 " ref={parentRef}>
        <div className={`${classElementFixed}`}>
          <div className="min-h-screen  bg-gradient-to-r from-[#6376d4]  to-[#2a1a85e7]  ">
            <div className="max-h-auto" id="parent-menu" dir="rtl">
              <div className="text-center p-2 font-bold text-lg text-back-3">
                javascript
              </div>
              <div className="grid grid-flow-col auto-cols-max">
                <DropDownMenu />
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className=" basis-1/2 h-[2100px]">
        <div className="relative">
          <p>new</p>
        </div>
      </div>
    </div>
  );
};

export default Article;

import Slider from "../components/sliders/slider";
import SliderGullary from "../components/sliders/slidergullary";
import Gullary from "../components/sliders/qullary";

export default function Home() {
  return (
    <>
      <Slider />
      <Gullary />
      <SliderGullary />
    </>
  );
}

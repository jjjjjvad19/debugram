import { Main, Head, NextScript, Html } from "next/document";
const Document = () => {
  return (
    <>
      <Html className="scroll-smooth">
        <Head></Head>
        <body>
          <Main></Main>

          <NextScript />
        </body>
        <link
          rel="stylesheet"
          type="text/css"
          charset="UTF-8"
          href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.css"
        />
        <link
          rel="stylesheet"
          type="text/css"
          href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick-theme.min.css"
        />
      </Html>
    </>
  );
};

export default Document;

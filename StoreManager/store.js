import Reducers from "./Reducers";
import { persistStore, persistReducer } from "redux-persist";
import { configureStore } from "@reduxjs/toolkit";
import storage from "redux-persist/lib/storage";
import thunk from "redux-thunk";

const persistConfig = {
  key: "root",
  storage,
  blacklist: ["reducerDevice"],
  whitelist: ["reducerUser"],
};

const persistedReducer = persistReducer(persistConfig, Reducers);
const store = configureStore({
  reducer: persistedReducer,
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({ serializableCheck: false, thunk }),
});
export const persistor = persistStore(store);
export default store;

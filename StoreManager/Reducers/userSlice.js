import { createSlice } from "@reduxjs/toolkit";

const store = createSlice({
  name: "UserSlice",
  initialState: {
    user: null,
    token: null,
  },
  reducers: {
    login: (state, data) => {
      state.user = data.payload.user;
      state.token = data.payload.token;
    },
    logOut: (state) => {
      state.token = null;
      state.user = null;
    },
  },
});
export const { login, logOut } = store.actions;

export default store.reducer;

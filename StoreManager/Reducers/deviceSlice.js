import { configureStore, createSlice } from "@reduxjs/toolkit";

const store = createSlice({
  name: "Device",
  initialState: {
    deviceMode: null,
  },
  reducers: {
    $watched: (state, data) => {
      state.deviceMode = data.payload;
    },
  },
});

export const { $watched } = store.actions;

export default store.reducer;

import reducerDevice from "./deviceSlice";
import reducerUser from "./userSlice";
import { combineReducers } from "redux";

const reducers = combineReducers({
  reducerDevice,
  reducerUser,
});
export default reducers;

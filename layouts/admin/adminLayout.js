import { useState } from "react";
import HeaderAdmin from "../../components/headers/headerAdmin";
import SidebarAdmin from "../../components/sidebar/sidebarAdmin";

const LayoutAdmin = ({ children }) => {
  const [toggleSidebar, setToggle] = useState(false);
  const clickMenuSidebar = () => {
    setToggle(!toggleSidebar);
  };
  return (
    <>
      <HeaderAdmin onClick={clickMenuSidebar} />
      <div className="flex gap-x-1 items-start">
        <div
          className={`transition-all duration-100 max-w-18%  h-100% min-h-screen  relative 
      ${toggleSidebar ? "w-64" : "w-16"}`}
        >
          <SidebarAdmin toggle={toggleSidebar} />
        </div>
        <div
          className={`transition-all duration-100 max-w-82%  ${
            !toggleSidebar ? "max-w-100%" : "max-w-83%"
          } h-100% w-100%`}
        >
          {children}
        </div>
      </div>
    </>
  );
};

export default LayoutAdmin;

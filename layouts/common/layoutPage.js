import HeaderPage from "../../components/headers/headerPage";
import FooterPage from "../../components/footers/footerPage";
import styles from "../../styles/layouts/layoutPage.module.scss";
const LayoutPage = ({ children }) => {
  return (
    <>
      <div
        className={`
        ${styles.layoutPage__main}
        h-100%
        after:absolute
        after:w-100%
        after:h-80
        after:top-0
        after:-z-20
        before:absolute 
        before:w-100% 
        before:h-100%
         before:-z-10 
         before:bg-back-4
         before:blur-md
         before:backdrop-blur
         before:shadow-inner
         before:opacity-50`}
      >
        <HeaderPage />
        <main className="">{children}</main>
        <FooterPage />
      </div>
    </>
  );
};

export default LayoutPage;
